module Part2 where

import Part2.Types
import Data.Maybe

------------------------------------------------------------
-- PROBLEM #6
--
-- Написать функцию, которая преобразует значение типа
-- ColorLetter в символ, равный первой букве значения
prob6 :: ColorLetter -> Char
prob6 colorLetter = case colorLetter of
  RED -> 'R'
  GREEN -> 'G'
  BLUE -> 'B'

------------------------------------------------------------
-- PROBLEM #7
--
-- Написать функцию, которая проверяет, что значения
-- находятся в диапазоне от 0 до 255 (границы входят)
prob7 :: ColorPart -> Bool
prob7 colorPart = validateColorValue  (prob9 colorPart)
    where validateColorValue colorValue = colorValue >= 0 && colorValue <= 255

------------------------------------------------------------
-- PROBLEM #8
--
-- Написать функцию, которая добавляет в соответствующее
-- поле значения Color значение из ColorPart
prob8 :: Color -> ColorPart -> Color
prob8 color colorPart = case colorPart of
  Red colorValue -> color { red = colorValue + (red color) }
  Blue colorValue -> color { blue = colorValue + (blue color) }
  Green colorValue -> color { green = colorValue + (green color) }

------------------------------------------------------------
-- PROBLEM #9
--
-- Написать функцию, которая возвращает значение из
-- ColorPart
prob9 :: ColorPart -> Int
prob9 colorPart = case colorPart of
  Red colorValue -> colorValue
  Blue colorValue -> colorValue
  Green colorValue -> colorValue

------------------------------------------------------------
-- PROBLEM #10
--
-- Написать функцию, которая возвращает компонент Color, у
-- которого наибольшее значение (если такой единственный)
prob10 :: Color -> Maybe ColorPart
prob10 color | red color > blue color &&  red color > green color = Just (Red (red color))
             | blue color > red color &&  blue color > green color = Just (Blue (blue color))
             | green color > blue color &&  green color > red color = Just (Green (green color))
             | otherwise = Nothing
------------------------------------------------------------
-- PROBLEM #11
--
-- Найти сумму элементов дерева
prob11 :: Num a => Tree a -> a
prob11 tree = maybe 0 prob11 (left tree) + (root tree) + maybe 0 prob11 (right tree)

------------------------------------------------------------
-- PROBLEM #12
--
-- Проверить, что дерево является деревом поиска
-- (в дереве поиска для каждого узла выполняется, что все
-- элементы левого поддерева узла меньше элемента в узле,
-- а все элементы правого поддерева -- не меньше элемента
-- в узле)
prob12 :: Ord a => Tree a -> Bool
prob12 tree =  checkBinLeftSubtree (left tree) (root tree) && checkBinRightSubtree (right tree) (root tree)
  where 
    checkBinLeftSubtree Nothing value  = True
    checkBinLeftSubtree (Just tree) value = root tree < value && prob12 tree

    checkBinRightSubtree Nothing value  = True
    checkBinRightSubtree (Just tree) value = root tree >= value && prob12 tree

------------------------------------------------------------
-- PROBLEM #13
--
-- На вход подаётся значение и дерево поиска. Вернуть то
-- поддерево, в корне которого находится значение, если оно
-- есть в дереве поиска; если его нет - вернуть Nothing
prob13 :: Ord a => a -> Tree a -> Maybe (Tree a)
prob13 value tree = findElementInBinaryTree value (Just tree)

findElementInBinaryTree :: Ord a => a -> Maybe (Tree a) -> Maybe (Tree a)
findElementInBinaryTree value Nothing = Nothing
findElementInBinaryTree value (Just tree) | root tree < value  = findElementInBinaryTree value (right tree)
                                          | root tree > value  = findElementInBinaryTree value (left tree)
                                          | value == root tree = Just (tree)

------------------------------------------------------------
-- PROBLEM #14
--
-- Заменить () на числа в порядке обхода "правый, левый,
-- корень", начиная с 1
prob14 :: Tree () -> Tree Int
prob14 tree = enumerate tree (getNodesCount tree)

getNodesCount :: Tree () -> Int
getNodesCount tree = 1 + maybe 0 getNodesCount (left tree) + maybe 0 getNodesCount (right tree)

getNumberForRightTree :: Tree () -> Int -> Int
getNumberForRightTree tree num = case left tree of
    Just leftSubTree -> num - (getNodesCount leftSubTree + 1)
    Nothing -> num - 1

enumerate :: Tree () -> Int -> Tree Int
enumerate tree num =
  Tree (do
     leftSubTree <- left tree
     return (enumerate leftSubTree (num - 1))
  )
  num
  (do
    rightSubTree <- right tree
    return (enumerate rightSubTree (getNumberForRightTree tree num))
  )

------------------------------------------------------------
-- PROBLEM #15
--
-- Выполнить вращение дерева влево относительно корня
-- (https://en.wikipedia.org/wiki/Tree_rotation)
prob15 :: Tree a -> Tree a
prob15 tree = maybe tree rotateLeft (right tree)
  where rotateLeft subtree = subtree {left = Just tree {right = left subtree}}

------------------------------------------------------------
-- PROBLEM #16
--
-- Выполнить вращение дерева вправо относительно корня
-- (https://en.wikipedia.org/wiki/Tree_rotation)
prob16 :: Tree a -> Tree a
prob16 tree = maybe tree rotateRight (left tree)
  where rotateRight subtree = subtree {right = Just tree {left = right subtree}}

------------------------------------------------------------
-- PROBLEM #17
--
-- Сбалансировать дерево поиска так, чтобы для любого узла
-- разница высот поддеревьев не превосходила по модулю 1
-- (например, преобразовать в полное бинарное дерево)
prob17 :: Tree a -> Tree a
prob17 tree = fromMaybe tree (balanceTree (convertToList tree))

convertToList :: Tree a -> [a]
convertToList tree = maybe [] convertToList (left tree) ++ [root tree]
                     ++ maybe [] convertToList (right tree)

balanceTree :: [a] -> Maybe (Tree a)
balanceTree [] = Nothing
balanceTree nodes = Just (Tree
    (balanceTree $ take half nodes)
    (nodes !! half)
    (balanceTree $ drop (half + 1) nodes))
  where
    half = length nodes `quot` 2

