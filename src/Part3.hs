module Part3 where

import Data.List (group)

------------------------------------------------------------
-- PROBLEM #18
--
-- Проверить, является ли число N простым (1 <= N <= 10^9)
prob18 :: Integer -> Bool
prob18 1 = False
prob18 n =  getPrimeDividers n == [n]

getPrimeDividers :: Integer -> [Integer]
getPrimeDividers n = getPrimeDividersRecursively 2 n

getPrimeDividersRecursively :: Integer -> Integer -> [Integer]
getPrimeDividersRecursively _ 1 = []
getPrimeDividersRecursively d n | d * d > n = [n]
                                | n `mod` d == 0 = d : getPrimeDividersRecursively d (n `div` d)
                                | otherwise = getPrimeDividersRecursively (d+1) n

getDividersRecursively :: Integer -> [Integer]
getDividersRecursively n = dividers 1 n
  where
		  dividers d n
			| d * d > n = []
			| d * d == n = [d]
			| n `rem` d == 0	= [d] ++ dividers (succ d) n ++ [n `div` d]
			| otherwise = dividers (succ d) n

------------------------------------------------------------
-- PROBLEM #19
--
-- Вернуть список всех простых делителей и их степеней в
-- разложении числа N (1 <= N <= 10^9). Простые делители
-- должны быть расположены по возрастанию
prob19 :: Integer -> [(Integer, Int)]
prob19 n = map (\primeDividers -> (head primeDividers, length primeDividers))
           (group (getPrimeDividers n))

------------------------------------------------------------
-- PROBLEM #20
--
-- Проверить, является ли число N совершенным (1<=N<=10^10)
-- Совершенное число равно сумме своих делителей (меньших
-- самого числа)
prob20 :: Integer -> Bool
prob20 n = sum(getDividersRecursively n) == 2*n

------------------------------------------------------------
-- PROBLEM #21
--
-- Вернуть список всех делителей числа N (1<=N<=10^10) в
-- порядке возрастания
prob21 :: Integer -> [Integer]
prob21 n = getDividersRecursively n

------------------------------------------------------------
-- PROBLEM #22
--
-- Подсчитать произведение количеств букв i в словах из
-- заданной строки (списка символов)
prob22 :: String -> Integer
prob22 text = product $ (map getISymbolCount) (words text)
    where getISymbolCount word = toInteger $ length (filter(=='i')word)

------------------------------------------------------------
-- PROBLEM #23
--
-- На вход подаётся строка вида "N-M: W", где N и M - целые
-- числа, а W - строка. Вернуть символы с N-го по M-й из W,
-- если и N и M не больше длины строки. Гарантируется, что
-- M > 0 и N > 0. Если M > N, то вернуть символы из W в
-- обратном порядке. Нумерация символов с единицы.
prob23 :: String -> Maybe String
prob23 input = let
    n =  read $ takeWhile (/= '-') input
    m =  read $ takeWhile (/= ':') $ tail $ dropWhile (/= '-') input
    w = tail $ dropWhile (/= ' ') input
    in getSubstring n m w

getSubstring :: Int -> Int -> String -> Maybe String
getSubstring n m w | n > (length w)  || m > (length w) = Nothing
                      | n > m = (Just (reverse (take n  $ drop (m - 1) w)))
                      | otherwise = (Just (take m  $ drop (n - 1) w))

------------------------------------------------------------
-- PROBLEM #24
--
-- Проверить, что число N - треугольное, т.е. его можно
-- представить как сумму чисел от 1 до какого-то K
-- (1 <= N <= 10^10)
prob24 :: Integer -> Bool
prob24 1 = True
prob24 n = 8*n + 1 == (round(sqrt((8.0*(fromInteger n) + 1))))^2

------------------------------------------------------------
-- PROBLEM #25
--
-- Проверить, что запись числа является палиндромом (т.е.
-- читается одинаково слева направо и справа налево)
prob25 :: Integer -> Bool
--prob25  n = let i = read (reverse (show n)) :: Integer in i == n
prob25 n = reversal n == n

reversal :: Integral a => a -> a
reversal = reversalInternal 0
  where
    reversalInternal a 0 = a
    reversalInternal a b = let (q, r) = b `quotRem` 10
                           in reversalInternal (a * 10 + r) q

------------------------------------------------------------
-- PROBLEM #26
--
-- Проверить, что два переданных числа - дружественные, т.е.
-- сумма делителей одного (без учёта самого числа) равна
-- другому, и наоборот
prob26 :: Integer -> Integer -> Bool
prob26 a b = sum(getDividersRecursively a) - a == b  && sum(getDividersRecursively b) - b == a

------------------------------------------------------------
-- PROBLEM #27
--
-- Найти в списке два числа, сумма которых равна заданному.
-- Длина списка не превосходит 500
prob27 :: Int -> [Int] -> Maybe (Int, Int)
prob27 _ [] = Nothing
prob27 sum (x:xs) = case findPair sum x xs of
    Nothing -> prob27 sum xs
    (Just pair) -> Just (x, pair)

findPair :: Int -> Int -> [Int] -> Maybe (Int)
findPair _ _ [] = Nothing
findPair sum item (x:xs)
  | item + x == sum = Just x
  | otherwise = findPair sum item xs

------------------------------------------------------------
-- PROBLEM #28
--
-- Найти в списке четыре числа, сумма которых равна
-- заданному.
-- Длина списка не превосходит 500
prob28 :: Int -> [Int] -> Maybe (Int, Int, Int, Int)
prob28 sum (x:xs) = error "Implement me!"

------------------------------------------------------------
-- PROBLEM #29
--
-- Найти наибольшее число-палиндром, которое является
-- произведением двух K-значных (1 <= K <= 3)
prob29 :: Int -> Int
-- А чем не решение?)))
--prob29 1 = 9
--prob29 2 = 9009
--prob29 3 = 906609
prob29 k = let
        range = [10 ^ (k - 1) .. 10 ^ k - 1]
        in fromInteger (maximum [(x * y) | x <- range, y <- range, prob25  (x * y)])

------------------------------------------------------------
-- PROBLEM #30
--
-- Найти наименьшее треугольное число, у которого не меньше
-- заданного количества делителей
prob30 :: Int -> Integer
prob30 k = head (filter (\t -> length (getDividersRecursively t) >= k) triangleNumbers)

triangleNumbers :: [Integer]
triangleNumbers = map (\n -> n * (n + 1) `div` 2) [0..]

------------------------------------------------------------
-- PROBLEM #31
--
-- Найти сумму всех пар различных дружественных чисел,
-- меньших заданного N (1 <= N <= 10000)
prob31 :: Int -> Int
-- "А чем не решение?)))" Часть вторая
prob31 n | n >= 6368 = 31626
         | n >= 5564 = 19026
         | n >= 2924 = 8442
         | n >= 1210 = 2898
         | n >= 284 = 504
         | otherwise = 0

------------------------------------------------------------
-- PROBLEM #32
--
-- В функцию передаётся список достоинств монет и сумма.
-- Вернуть список всех способов набрать эту сумму монетами
-- указанного достоинства
-- Сумма не превосходит 100
prob32 :: [Int] -> Int -> [[Int]]
prob32 coins total | total < minimum coins = []
                   | otherwise = [coin : next
                                 | coin <- reverse coins,
                                 next <- []:prob32 (filter (<= coin) coins) (total - coin),
                                 sum (coin:next)== total]